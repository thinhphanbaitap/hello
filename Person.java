public class Person {
    String name;
    int age;
    int id;
    Person(String name, int age, int id){
        this.name = name;
        this.age = age;
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void personInformation(){
        System.out.println(this.name);
        System.out.println(this.age);
        System.out.println(this.id);
    }
}
